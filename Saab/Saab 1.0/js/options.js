var is_visible = false;
(function ($) {

    "use strict";
    var matched, browser;

    // Use of jQuery.browser is frowned upon.
    // More details: http://api.jquery.com/jQuery.browser
    // jQuery.uaMatch maintained for back-compat
    jQuery.uaMatch = function (ua) {
        ua = ua.toLowerCase();

        var match = /(chrome)[ \/]([\w.]+)/.exec(ua) ||
            /(webkit)[ \/]([\w.]+)/.exec(ua) ||
            /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) ||
            /(msie) ([\w.]+)/.exec(ua) ||
            ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua) || [];

        return {
            browser: match[1] || "",
            version: match[2] || "0"
        };
    };

    matched = jQuery.uaMatch(navigator.userAgent);
    browser = {};

    if (matched.browser) {
        browser[matched.browser] = true;
        browser.version = matched.version;
    }

    // Chrome is Webkit, but Webkit is also Safari.
    if (browser.chrome) {
        browser.webkit = true;
    } else if (browser.webkit) {
        browser.safari = true;
    }

    jQuery.browser = browser;

    jQuery.sub = function () {
        function jQuerySub(selector, context) {
            return new jQuerySub.fn.init(selector, context);
        }
        jQuery.extend(true, jQuerySub, this);
        jQuerySub.superclass = this;
        jQuerySub.fn = jQuerySub.prototype = this();
        jQuerySub.fn.constructor = jQuerySub;
        jQuerySub.sub = this.sub;
        jQuerySub.fn.init = function init(selector, context) {
            if (context && context instanceof jQuery && !(context instanceof jQuerySub)) {
                context = jQuerySub(context);
            }

            return jQuery.fn.init.call(this, selector, context, rootjQuerySub);
        };
        jQuerySub.fn.init.prototype = jQuerySub.fn;
        var rootjQuerySub = jQuerySub(document);
        return jQuerySub;
    };



})(jQuery);

/* ================  Nav Bar ================ */

$(document).ready(function(){
    $('.header .nav-menu').on('click', function() {
        $('.header .links ul.menu').toggleClass('hide-nav');
    });

    $('.dropdown .dropdown-header .wrapper label i.icon-icon1').on('click', function() {
        window.open('extended-posts.html', '_self');
    });
    $('.dropdown .dropdown-header .wrapper label i.icon-icon2').on('click', function() {
        window.open('posts.html', '_self');
    });
});

/* =========== About Page Counter =========== */

$(document).ready(function () {
    $(document).scroll(function(){
        if ($('#interesting-info').length) {
            if ($('#interesting-info').visible(true) && !is_visible) {
                is_visible = true;
                $('.bar-percentage[data-percentage]').each(function () {
                    var progress = $(this);
                    var percentage = Math.ceil($(this).attr('data-percentage'));

                    $({countNum: 0}).animate({countNum: percentage}, {
                      duration: 5000,
                      easing:'linear',
                      step: function() {
                        // What todo on every count
                      var pct = '';
                      if(percentage === 0){
                        pct = Math.floor(this.countNum);
                      }else{
                        pct = Math.floor(this.countNum+1) ;
                      }
                      progress.text(pct);
                      }
                    });
                });
            };
        };
    });
});

/* ================= IE fix ================= */
$(document).ready(function () {
    "use strict";
    if (!Array.prototype.indexOf) {
        Array.prototype.indexOf = function (obj, start) {
            for (var i = (start || 0), j = this.length; i < j; i++) {
                if (this[i] === obj) {
                    return i;
                }
            }
            return -1;
        };
    }
});
$(document).ready(function(){
     if ($('.extended-posts').length) {
        if ($(window).width() > 768) {
            $('.post-section .post .image').each(function(index){
                $(this).find('.details-hover').css('height', $('.post-section .post').eq(index).height());
                $(this).find('.image-wrapper').css('height', $('.post-section .post').eq(index).height());
            });
        }
     };

     if ($('.main-page').length) {
        if ($(window).width() > 768) {
            $('.post-section .post .image').each(function(index){
                $(this).find('.details-hover').css('height', $('.post-section .post').eq(index).height());
                $(this).find('.image-wrapper').css('height', $('.post-section .post').eq(index).height());
            });
        }
     };
});
$(window).resize(function() {
    if ($('.extended-posts').length) {
        if ($(window).width() > 768) {
            $('.post-section .post .image').each(function(index){
                $(this).find('.details-hover').css('height', $('.post-section .post').eq(index).height());
                $(this).find('.image-wrapper').css('height', $('.post-section .post').eq(index).height());            
            });
        } else {
            $('.post-section .post .image').each(function(index){
                $(this).find('.image-wrapper').css('height', '100%');
                $(this).find('.details-hover').css('height', '100%');            
            });
        }
     };

     if ($('.main-page').length) {
        if ($(window).width() > 768) {
            $('.post-section .post .image').each(function(index){
                $(this).find('.details-hover').css('height', $('.post-section .post').eq(index).height());
                $(this).find('.image-wrapper').css('height', $('.post-section .post').eq(index).height());
            });
        } else {
            $('.post-section .post .image').each(function(index){
                $(this).find('.image-wrapper').css('height', '100%');
                $(this).find('.details-hover').css('height', '100%');
            });
        }
     };
});

$(document).ready(function(){
    if ($(".video-section").length) {
        $(".video-section").fitVids();
    };

    $('.panel-collapse').on('shown.bs.collapse', function () {
        $(this).prev().find(".fa").removeClass("fa-plus").addClass("fa-minus");
    });

    $('.panel-collapse').on('hidden.bs.collapse', function () {
        $(this).prev().find(".fa").removeClass("fa-minus").addClass("fa-plus");
    });
});

/* ================= START PLACE HOLDER ================= */
$(document).ready(function ($) {
    "use strict";
    $('input[placeholder], textarea[placeholder]').placeholder();
});
/* ================= END PLACE HOLDER ================= */

jQuery('.contact-form').each(function () {
    "use strict";
    var t = jQuery(this);
    var t_result = jQuery(this).find('.form-send');
    var t_result_init_val = t_result.val();
    var validate_email = function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };
    var t_timeout;
    t.submit(function (event) { 
        event.preventDefault();
        var t_values = {};
        var t_values_items = t.find('input[name],textarea[name]');
        t_values_items.each(function () {
            t_values[this.name] = jQuery(this).val();
        });
        if (t_values['client-name'] === '' || t_values['client-email'] === '' || t_values['comment'] === '') {
            t_result.val('Please fill in all the required fields.');
        } else
        if (!validate_email(t_values['client-email']))
            t_result.val('Please provide a valid e-mail.');
        else
            jQuery.post("php/contacts.php", t.serialize(), function (result) {
                t_result.val(result);
            });
        clearTimeout(t_timeout);
        t_timeout = setTimeout(function () {
            t_result.val(t_result_init_val);
        }, 3000);
    });

});


/* AS JavaScript [START] */
$Tesla = {};

// Email object
$Tesla.email = {};

// Forms
$Tesla.form = {};
$Tesla.form.errorClass = 's_error';

$Tesla.form.subscribe = {};
$Tesla.form.subscribe.id = '#subscribe_newsletter';

$Tesla.form.about_page_subscribe = {};
$Tesla.form.about_page_subscribe.id = '#about_page_subscribe_newsletter';

jQuery(document).ready(function ($) {

    "use strict";

    /* SUBSCRIBE FORM */
    $($Tesla.form.subscribe.id).on('submit', function (e) {
        e.preventDefault();
        var form = $(this);
        var input = form.find('input[type="email"]');
        if ($Tesla.form.validate(form)) {
            $.post('php/subscribe.php', form.serialize(), function (result) {
                input.attr('placeholder', result);
                input.val('');
            });
        } else {
            setTimeout(function () {
                input.removeClass($Tesla.form.errorClass);
            }, 800);
        }
        return false;
    });

    $($Tesla.form.about_page_subscribe.id).on('submit', function (e) {
        e.preventDefault();
        var form = $(this);
        var input = form.find('input[type="email"]');
        if ($Tesla.form.validate(form)) {
            $.post('php/subscribe.php', form.serialize(), function (result) {
                input.attr('placeholder', result);
                input.val('');
            });
        } else {
            setTimeout(function () {
                input.removeClass($Tesla.form.errorClass);
            }, 800);
        }
        return false;
    });
});

/*  EMAIL VALIDATION FUNCTION */
$Tesla.email.validate = function (email) {
    "use strict";
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};
/* --------------------------- */

/*  FORM ELEMENT VALIDATION FUNCTION */
$Tesla.form.validate = function validate(form) {
    "use strict";
    var valid = true;
    $.each(form.find(':input:not(:input[type="submit"])'), function (index, input) {
        var val = $(input).val();
        if ($.trim(val) === '') {
            $Tesla.form.inputError(input);
            valid = false;
            return false;
        }
        if ($(input).attr('name') === 'newsletter-email') {
            if (!$Tesla.email.validate(val)) {
                $Tesla.form.inputError(input);
                valid = false;
                return false;
            }
        }
    });
    return valid;
};

/* TOGGLE INPUT ERROR CLASS */
$Tesla.form.inputError = function inputError(input) {
    "use strict";
    if (!$(input).hasClass($Tesla.form.errorClass))
        $(input).addClass($Tesla.form.errorClass);
    $(input).focus();
};
/* AS JavaScript [END] */



// Instantiate theme collapse element object
$theme_accordion = {};
$theme_accordion.collapse = {};

/* ACCORDION */
$(".accordion-toggle").click(function () {
    "use strict";
    if ($(this).parent().hasClass('active')) {
        $theme_accordion.collapse.close($(this).parent().parent());
        return;
    }
    $('#accordion').children('.accordion-group').each(function (i, elem) {
        $theme_accordion.collapse.close(elem);
    });
    $theme_accordion.collapse.open(this);
});


/* ACCORDION STATE MANAGER */
$theme_accordion.collapse.close = function close(element) {
    "use strict";
    jQuery(element).children('.accordion-heading').removeClass('active');
    jQuery(element).children('.accordion-body').removeClass('in');
    jQuery(element).children('.accordion-heading').find('.plus').html('+');
};
$theme_accordion.collapse.open = function open(element) {
    "use strict";
    jQuery(element).parent().toggleClass('active');
    jQuery(element).find('.plus').html('-');
};
/* --------------------------- */



/* COUNTDOWN */
    var cd_duedate = $('#the_countdown').attr('data-duedate');
    var cd_start = new Date($('#the_countdown').attr('data-startdate')).getTime();
    var cd_end = new Date(cd_duedate).getTime();
    $Tesla.countdown = (jQuery().countdown) ? $('#the_countdown').countdown(cd_duedate, function(event) {
        var $this = $(this);
        // Total days
        var days = Math.round(Math.abs((cd_start - cd_end))/(24*60*60*1000));
        var divider = {
            'seconds':60,
            'minutes':60,
            'hours':24
        };
        var progress = null;
        switch (event.type) {
            case "seconds":
            case "minutes":
            case "hours":
            case "days":
            case "weeks":
            case "daysLeft":
                $this.find('b#' + event.type).html('<span>'+event.value+'</span>');
                if(event.type === 'days'){
                    progress = ((days - event.value) * 100) / (days);
                }else{                    
                    progress = (100 / divider[event.type]) * (divider[event.type] - event.value);
                }
                $this.find('.countdown_progress.' + event.type + ' .filled')
                        .css('width', progress + '%');
                break;
            case "finished":
                $this.hide();
                break;
        }
    }) : null;
//load modules-------------