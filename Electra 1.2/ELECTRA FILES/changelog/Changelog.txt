Version 1.2 - Updated: 06.09.2013
================================================
Fixed : Responsive for Slider
Fixed : Responsive for Top menu
Added : +2 Home Pages


Version 1.1 - Updated: 05.09.2013
================================================
Fixed : Minor style fixes
Fixed : Additional spacing for OUR CLIENTS section
Added : +1 Home Pages
Added : Color Picker for changing Site color


Version 1.0 - Updated: 01.09.2013
================================================
Initial Release