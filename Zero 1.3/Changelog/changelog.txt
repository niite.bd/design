Version 1.1 - 04/09/2014
================================================
Fixed	:	Animation scroll - masonry issue ( options.js - calling gridEffect() after queryLoader(); )

Version 1.2 - 04/10/2014
================================================
Fixed	:	Imgeloaded fix Chrome bug via setTimeout()
Added	:	Added scroll animations

Version 1.3 - 04/11/2014
================================================
Fixed	:	The site can now be loaded locally (file://)