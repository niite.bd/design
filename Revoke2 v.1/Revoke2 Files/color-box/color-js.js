// Limit scope pollution from any deprecated API
jQuery(function($){

    var load_changer = function(){
        var t_body = $('body');
        var t_div = $('.boxed-fluid');
        var t_transition_time = 0;
        var t_transition_apply = function(){
            clearTimeout(t_transition_time);
            t_body.addClass('animated_change');
            t_transition_time = setTimeout(t_transition_end, 1500);
        };
        var t_transition_end = function(){
            t_body.removeClass('animated_change');
        };
        var t_color = $('select[name="site_color"]');
        var t_layout = $('select[name="site_layout"]');
        t_color.change(function(){
            var t = $(this);
            if(t.val()!==t.data('color')){
                t_transition_apply();
                switch(t.val()){
                    case 'dark':
                        t_div.addClass('black_version');
                        break;
                    case 'light':
                        t_div.removeClass('black_version');
                        break;
                    default:
                        break;
                }
                t.data('color', t.val());
            }
        });
        t_layout.change(function(){
            var t = $(this);
            if(t.val()!==t.data('layout')){
                t_transition_apply();
                switch(t.val()){
                    case 'boxed':
                        t_body.addClass('boxed');
                        break;
                    case 'wide':
                        t_body.removeClass('boxed');
                        break;
                    default:
                        break;
                }
                t.data('layout', t.val());
            }
        });
        $('#background_patterns>li').click(function(){
            if('boxed'!==t_layout.data('layout')){
                t_layout.val('boxed');
                t_transition_apply();
                t_body.addClass('boxed');
                t_layout.data('layout','boxed');
            }
            t_body.css({backgroundImage: 'url('+$(this).children('img').attr('src')+')', backgroundRepeat: 'repeat'});
        });
        $('#background_images>li').click(function(){
            if('boxed'!==t_layout.data('layout')){
                t_layout.val('boxed');
                t_transition_apply();
                t_body.addClass('boxed');
                t_layout.data('layout','boxed');
            }
            t_body.css({backgroundImage: 'url('+$(this).children('img').attr('src')+')', backgroundRepeat: 'no-repeat'});
        });

        var t_box = $('.color-box');
        var t_box_width = t_box.outerWidth();
        $('.color-box_settings').click(function(){
            if(t_box.data('visible')){
                t_box.css({left: -t_box_width});
                t_box.data('visible', false);
            }else{
                t_box.css({left: 0});
                t_box.data('visible', true);
            }
        });

        var style_index = 0;

        function color_options(selector, default_color, color_change){
            var t_color = default_color;
            var t_picker = undefined;
            var t_picker_container = $('<div/>').addClass('color_picker').css({position: 'fixed', zIndex: 999, top: 388, left: 200});
            var t_color_input = $('<input/>').css({position: 'absolute', zIndex: 999, top: 198, left: 0, width: '100%', textAlign: 'center', lineHeight: '1.6em', border: '1px solid black'});
            var style_id = 'color-box-style'+style_index;
            style_index++;
            $('.color-box_settings').click(function(){
                if(t_box.data('visible')){
                    t_picker_container.hide();
                }
            });
            var t_callback = function(){
                var t_style = $('head>#'+style_id);
                var t_output = '';
                if(!t_style.length)
                    t_style = $('<style/>').attr('id',style_id).appendTo('head');
                t_output += color_change.replace(/%color%/g,t_color);
                t_style.html(t_output);
            };
            $(selector+'>li:lt(-1)').click(function(){
                $('.color_picker').hide();
                t_color = $(this).children('span').css('background-color').replace(/rgb\(\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*\)/ig,function(c, r, g, b){return '#'+Number(r).toString(16)+Number(g).toString(16)+Number(b).toString(16);});
                t_callback();
                if(undefined!==t_picker){
                    t_picker.setColor(t_color);
                    t_color_input.val(t_color);
                }
            });
            var t_input_update = function(){
                t_color_input.val(t_color);
                t_color_input.css({backgroundColor: t_color, color: t_picker.hsl[2] > 0.5 ? '#000' : '#fff'});
            };
            $(selector+'>li:last').click(function(){
                if(undefined===t_picker){
                    t_picker = $.farbtastic(t_picker_container.appendTo('body'));
                    t_picker_container.append(t_color_input);
                    t_picker.setColor(t_color);
                    t_input_update();
                    t_picker.linkTo(function(color){
                        t_color = color;
                        t_callback();
                        t_input_update();
                    });
                    t_color_input.change(function(){
                        t_picker.setColor(t_color_input.val());
                    });
                    t_picker_container.show();
                }else{
                    $('.color_picker').not(t_picker_container).hide();
                    t_picker_container.toggle();
                }
            });
        }

        color_options('#site_color','#ff5113',"\n\
        .d-text-c.active,\n\
        .d-text-c-h.active,\n\
        .header-top-socials li a:hover,\n\
        .twitter_widget ul li a:hover,\n\
        .d-text-c-h:hover,\n\
        .header-top-info li a:hover,\n\
        .latest-widget li h6 a:hover,\n\
        .comment-area-v1 .comments-section .comment .comment-author a:hover,\n\
        .footer .bottom-footer a,\n\
        .main-sidebar .widget-contact a:hover,\n\
        .blog-post .share-this-post li a:hover,\n\
        .project-section .project-data li a,\n\
        .billing-form a:hover,\n\
        .product-tabs .nav-tabs > li.active > a,\n\
        .product-tabs .nav-tabs > li.active > a:hover,\n\
        .product-tabs .nav-tabs > li.active > a:focus,\n\
        .main-sidebar .widget ul li a:hover,\n\
        .cart-page .panel-heading a:hover,\n\
        .d-text-c {\n\
            color: %color% !important;\n\
        }\n\
        .d-bg-c.active,\n\
        .filter-area .filter-box .filter li a.active,\n\
        .main-nav ul li.active>a,\n\
        .top-cart .cart-qty,\n\
        .top-cart a:hover,\n\
        footer .widget .flickr-widget li a:hover,\n\
        .main-nav ul li a:hover,\n\
        .d-bg-c-h:hover,\n\
        .mini-slider .slider-dots li.active,\n\
        .product-box .product-addcart:hover i,\n\
        .footer .widget .flickr_widget li a:hover,\n\
        .our-team-section .team-member .member-image,\n\
        .features-ul-right li i,\n\
        .features-ul-left li i,\n\
        .main-sidebar .widget-categories ul li:hover a:before,\n\
        .d-bg-c-h.active,\n\
        .comment-area-v1 .comment-form .comment-button:hover,\n\
        .product-section .product-cover .the-slider .the-bullets-dots li.active,\n\
        .d-bg-c {\n\
            background: %color% !important;\n\
        }\n\
        .d-border-c.active,\n\
        .d-border-c-h:hover,\n\
        .site-title-right span,\n\
        .d-border-c-h.active,\n\
        .main-sidebar .widget-recent-works ul li a:hover,\n\
        .product-section .product-cover .the-slider .the-bullets-dots li.active,\n\
        .site-title-right,\n\
        .product-tabs .nav-tabs > li.active > a,\n\
        .product-tabs .nav-tabs > li.active > a:hover,\n\
        .product-tabs .nav-tabs > li.active > a:focus,\n\
        .main-sidebar .widget-gallery ul li a:hover,\n\
        .footer .widget .widget-title span,\n\
        .footer .widget .widget-title,\n\
        .site-title,\n\
        .site-title span,\n\
        .testimonial .testimonial-image img,\n\
        .footer .bottom-footer p a:hover,\n\
        .d-border-c {\n\
            border-color: %color% !important;\n\
        }\n\
        ");

        color_options('#site_color_2','#6694c7',"\n\
        .s-text-c.active,\n\
        .s-text-c-h.active,\n\
        .s-text-c-h:hover,\n\
        .s-text-c {\n\
            color: %color% !important;\n\
        }\n\
        .s-bg-c.active,\n\
        .s-bg-c-h:hover,\n\
        .s-bg-c-h.active,\n\
        .s-bg-c {\n\
            background: %color% !important;\n\
        }\n\
        .s-border-c.active,\n\
        .s-border-c-h:hover,\n\
        .s-border-c-h.active,\n\
        .s-border-c {\n\
            border-color: %color% !important;\n\
        }\n\
        ");

    };

    load_changer();

});