// Instantiate theme global javascript object
$Casablanca = Object();

// Instantiate theme collapse element object
$Casablanca.collapse = Object();
$Casablanca.services = Object();
$Casablanca.mobile = Object();

// Forms
$Casablanca.form = Object();
$Casablanca.form.errorClass = 'error';
$Casablanca.email = Object();

// Contact form
$Casablanca.form.contact = Object()
$Casablanca.form.contact.id = '#contact_form'

// Properties layout
$Casablanca.layout = Object();
// For how long the properties layout cookie will be saved (in days)
$Casablanca.layout.cookieExpire = 365;

jQuery(window).resize(function($) {
    $Casablanca.bg_mask();
    $Casablanca.services.height();
});

jQuery(document).ready(function($) {

    /* CHECKING FOR MOBILE DEVICES */
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        // DO IF MOBILE
        $Casablanca.mobile.menu();
    }

    /* FIXING SOLID BACKGROUND */
    $Casablanca.bg_mask();

    $('select#locations').selectr({placeholder: "Location", class: 'selectr'});
    $('select#beds').selectr({placeholder: "Beds", class: 'selectr'});
    $('select#baths').selectr({placeholder: "Baths", class: 'selectr'});
    $('form.agents_form select.agent_region').selectr({placeholder: "Search By Region", class: 'selectr'});
    $('form.agents_form select.agent_location').selectr({placeholder: "Search By Location", class: 'selectr'});

    /* ACCORDION */
    $("#cb_accordion .heading").click(function() {
        if ($(this).children('i.icon').hasClass('active')) {
            $Casablanca.collapse.close(this);
        } else {
            $Casablanca.collapse.open(this);
        }
    });

    /* CAROUSEL */
    $('#cb_slider').carousel({interval: 5000});

    $('#cb_slider').on('slide.bs.carousel', function() {
        var figcaption = $(this).find('section.figcaption');
        var title = $(figcaption).find('h1.title');
        $(title).toggleClass('fadein');
    })

    $('#cb_slider').on('slid.bs.carousel', function() {
        var title = $($(this).children('section.figcaption')).find('h1.title');
        var hidden = $(this).find('ul.cb_slider > li.active').find('figcaption');

        $(title).html($(hidden).html());
        $(title).animate({
            width: $(hidden).outerWidth() + 'px',
        }, 400);
        $(title).toggleClass('fadein');
    })

    /* CALCULATE AND SET CIRCLES HEIGHT*/
    $Casablanca.services.height();

    /* PARTNERS */
    $('.cb_partners > .arrow').click(function(e) {
        var slide = $(this).attr('data-slide');
        var list = $('.cb_partners ul.list');
        var li = $(list).children();
        var items = $(list).attr('data-items');
        var index = $(list).children('li.first').index();

        if (slide === 'next') {
            if (index < ($(li).length - items)) {
                $(li).removeClass('first');
                $(li).eq(index).addClass('left');
                $(li).eq(index + 1).addClass('first');
            } else {
                $(li).eq(index).addClass('no_right');
                setTimeout(function() {
                    $(li).eq(index).removeClass('no_right');
                }, 600);
            }
        } else if (slide === 'prev') {
            if (index != 0) {
                $(li).removeClass('first');
                $(li).eq(index - 1).removeClass('left');
                $(li).eq(index - 1).addClass('first');
            } else {
                $(li).eq(index).addClass('no_left');
                setTimeout(function() {
                    $(li).eq(index).removeClass('no_left');
                }, 600);
            }

        }
    });

    /* PROPERTY GALLERY */
    $('ul.cbg_thumbs > li.item a').click(function(e) {
        e.preventDefault();
        var src = $(this).find('img').attr('src');
        update(src);
        return false;
    });
    $('.cb_gallery > .arrow').click(function(e) {
        var slide = $(this).attr('data-slide');
        var list = $('.cb_gallery ul.cbg_thumbs');
        var li = $(list).children();
        var items = $(list).attr('data-items');
        var index = $(list).children('li.first').index();
        var src;

        if (slide === 'bottom') {
            if (index < ($(li).length - items)) {
                $(li).removeClass('first');
                $(li).eq(index).addClass('top');
                $(li).eq(index + 1).addClass('first');

                // update main image
                src = $(li).eq(index + 1).find('img').attr('src');
                update(src);
            } else {
                $(li).eq(index).addClass('no_bottom');
                setTimeout(function() {
                    $(li).eq(index).removeClass('no_bottom');
                }, 600);
            }
        } else if (slide === 'top') {
            if (index != 0) {
                $(li).removeClass('first');
                $(li).eq(index - 1).removeClass('top');
                $(li).eq(index - 1).addClass('first');

                // update main image
                src = $(li).eq(index + 2).find('img').attr('src');
                update(src);
            } else {
                $(li).eq(index).addClass('no_top');
                setTimeout(function() {
                    $(li).eq(index).removeClass('no_top');
                }, 600);
            }
        }
    });

    /* PROPERTIES LAYOUT SWITCHER */
    $('ul.cb_thumbnails.cb_properties').removeClass('grid list').addClass($.cookie('layout'))
    if ($('ul.cb_thumbnails.cb_properties').hasClass('grid')) {
        $('#layout_switcher > a.grid').toggleClass('active')
    } else if ($('ul.cb_thumbnails.cb_properties').hasClass('list')) {
        $('#layout_switcher > a.list').toggleClass('active')
    }
    $('#layout_switcher > a').click(function(e) {
        e.preventDefault();
        var layout = $(this).attr('data-layout');
        if (!$(this).hasClass('active'))
            $('ul.cb_thumbnails').toggleClass('list grid');
        $(this).parent().children('a').removeClass('active');
        $(this).addClass('active');
        
        $.cookie('layout', layout, {expires: $Casablanca.layout.cookieExpire});

        return false;
    });

    /* FILTER SLIDER ELEMENT */
    var slidr = $("#slidr");
    slidr.slider({
        //animate: true,
        range: true,
        min: slidr.attr('data-min'),
        max: slidr.attr('data-max'),
        step: parseInt(slidr.attr('data-step')),
        values: [parseInt(slidr.attr('data-minval')), parseInt(slidr.attr('data-maxval'))],
        slide: function(event, ui) {
            $(ui.handle).attr('data-value', ui.value);
            slidr.find('input#slider_min').val(ui.values[0]);
            slidr.find('input#slider_max').val(ui.values[1]);
        },
        create: function(event, ui) {
            $('<input/>', {id: 'slider_min', type: 'hidden', name: 'min', value: slidr.slider('values', 0)}).appendTo(slidr);
            $('<input/>', {id: 'slider_max', type: 'hidden', name: 'max', value: slidr.slider('values', 1)}).appendTo(slidr);
        }
    });
    $(slidr).find('a:first').attr('data-value', slidr.slider('values', 0));
    $(slidr).find('a:last').attr('data-value', slidr.slider('values', 1));
    ruller(slidr);


    /* ================= CONTACT FORM ================= */
    $($Casablanca.form.contact.id).submit(function() {
        var form = $(this);
        if ($Casablanca.form.validate(form)) {
            $.post('php/contact.php', form.serialize(), function(result) {
                console.log(result);
            });
        }
        return false;
    });

});

$Casablanca.mobile.menu = function (){
    $('#cb_main_menu a').click(function(e){
        if($(this).parent().has('ul')){
            e.preventDefault();
            $(this).trigger('mouseover');
            return false;
        }
    });
}

$Casablanca.services.height = function height(){
    $('.cb_services > li.item').height($('.cb_services > li.item').width());
    $('.cb_services > li.service > section.type').height($('.cb_services > li.service > section.type').width());
};

/*  EMAIL VALIDATION FUNCTION */
$Casablanca.email.validate = function(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};
/* --------------------------- */

/*  FORM ELEMENT VALIDATION FUNCTION */
$Casablanca.form.validate = function validate(form) {
    var valid = true;
    $.each(form.find(':input'), function (index, input){
        var val = $(input).val();
        if($.trim(val) === ''){
            $Casablanca.form.inputError(input);
            valid = false;
            return false;
        }
        if($(input).attr('name') === 'email'){
            if(!$Casablanca.email.validate(val)){
                $Casablanca.form.inputError(input);
                valid = false;
                return false;
            }
        }
    });
    return valid;
};

$Casablanca.form.inputError = function inputError(input){
    if(!$(input).hasClass($Casablanca.form.errorClass))
        $(input).addClass($Casablanca.form.errorClass);
    $(input).focus();
};

function ruller(slidr) {
    var min = parseInt(slidr.attr('data-min'));
    var max = parseInt(slidr.attr('data-max'));
    var step = parseInt(slidr.attr('data-step'));
    var bars = (step !== 0) ? (max - min) / step : 5;
    var rullers = $('.custom_slider > ul.rulers');
    var item = rullers.find('li.item:first-child');
    for (var i = 0; i < bars; i++) {
        var bar = item.clone(),
                step = (step === 0) ? max / bars : step,
                val = formatNumber((i * step) + step),
                left = (100 / bars) * (i + 1),
                b = bar.find('b.value');

        bar.css('left', left + '%').find('b.value').html(val);
        bar.appendTo(rullers);
        b.css('margin-left', (-b.width() / 2) + 'px');
    }
}

function formatNumber(n) {
    var ranges = [
        {divider: 1e18, suffix: 'P'},
        {divider: 1e15, suffix: 'E'},
        {divider: 1e12, suffix: 'T'},
        {divider: 1e9, suffix: 'G'},
        {divider: 1e6, suffix: 'M'},
        {divider: 1e3, suffix: 'K'}
    ];
    for (var i = 0; i < ranges.length; i++) {
        if (n >= ranges[i].divider) {
            return (n / ranges[i].divider).toString() + ranges[i].suffix;
        }
    }
    return n.toString();
}

$Casablanca.bg_mask = function bg_mask() {
    var sb_pos = $('#sidebar').position();
    var sb_width = $('#sidebar').outerWidth(true);    
    $('#bg_mask').css('left', sb_pos.left + sb_width + 'px');
    
    var bg_width = $('#bg_mask').width()
    $('#cb_copyright').css('right', bg_width + 'px');
};

/* ACCORDION STATE MANAGER */
$Casablanca.collapse.close = function close(element) {
    jQuery(element).children('i.icon').removeClass('active');
    jQuery(element).children('i.icon').html('+');
};
$Casablanca.collapse.open = function open(element) {
    jQuery(element).find('i.icon').toggleClass('active');
    jQuery(element).find('i.icon').html('-');
};
/* --------------------------- */

function update(src) {
    $('.cbg_main > figure > img').fadeOut('slow', function() {
        $(this).attr('src', src).fadeIn();
    });
}
;


/* create custom select plugin */
(function($) {
    $.fn.selectr = function(opts) {
        var $this = $(this),
                options = $this.children('option'),
                opts = $.extend({
            // These are the defaults.
            class: '',
            type: 'list',
            placeholder: '',
            trigger: 'click',
        }, opts);


        // Hide select element
        $this.hide();

        // Create fake slector ul element
        var $selectr = $('<ul />', {
            class: opts.class
        });
        $this.after($selectr);
        $selectr = $this.next('ul.selectr');

        var placeholder = (opts.placeholder) ? opts.placeholder : options.first().text();
        var $list = $('<span />', {
            text: placeholder
        }).appendTo($selectr).wrap('<li></li>').after('<i></i>').parent().append('<ul></ul>');

        var $ul = $list.children('ul');
        return options.each(function(i, j) {
            $elem = $('<li />', {
                text: j.text
            }).appendTo($ul);

            $selectr.on(opts.trigger, function(e) {
                $ul.toggleClass('open');
            });

            $ul.children('li').click(function(e) {
                options.attr('selected', false).eq($(this).index()).attr('selected', 'selected');
                $list.children('span').text($(this).text());
            });

        });

    };
}(jQuery));